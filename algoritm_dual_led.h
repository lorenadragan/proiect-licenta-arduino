/*
int pin_photoresistor = A0; //pin la care e legat fotorezistorul
int pin_red_dual = 8; pin la care e legat culoarea rosie din led
int pin_green_dual = 9; pin la care e legat culoarea verde din led
int value_photoresistor = 0; //valoarea citita din photoresistor
boolean is_off_dual_led_photoresistor = false; //initial ledul este aprins
boolean is_auto_dual_led_photoresistor = true; //si este setat pe modul auto 
int value_red_intensity_dual = 255; // valorile pentru led, de unde vor fi setate cand sunt pe manual 
int value_green_intensity_dual = 255; //green 
int value_intensity_auto = 0; //valoarea pe care o ia ledul cand e auto 
int value_off =0;
int value_default = 255;


//photoresistor and dual led
  JsonObject dual_led_photoresistor = doc.createNestedObject("dual_led_light_sensitive");
  dual_led_photoresistor["is_off_dual_led_photoresistor"] = is_off_dual_led_photoresistor;
  dual_led_photoresistor["is_auto_dual_led_photoresistor"] = is_auto_dual_led_photoresistor;
  dual_led_photoresistor["value_red_intensity_dual"] = value_red_intensity_dual;
  dual_led_photoresistor["value_green_intensity_dual"] = value_green_intensity_dual;
  dual_led_photoresistor["value_intensity_auto"] = value_intensity_auto;
  
  
*/

void control_dual_led_with_extern_light_intensity(){
	if(is_off_dual_led_photoresistor == true){
		//ledul este stins, pun 0 pe rosu si verde 
		value_red_intensity_dual = value_off;
		value_green_intensity_dual = value_off;
		value_intensity_auto = value_off;
		analogWrite(pin_red_dual, value_off);
		analogWrite(pin_green_dual, value_off);
	}
	else {
		//ledul este aprins si poate functiona in doua moduri 
		if(is_auto_dual_led_photoresistor == true){
			value_intensity_auto = compute_value_for_dual_leds();
			value_intensity_auto = 255 - value_intensity_auto;
			
			//le setez pentru ca sunt puse mai departe in server
			value_red_intensity_dual = value_intensity_auto;
			value_green_intensity_dual = value_intensity_auto;
			
			analogWrite(pin_red_dual, value_red_intensity_dual);
			analogWrite(pin_green_dual, value_green_intensity_dual);
		}
		else {
			//nu este auto, primesc valorile din client
			analogWrite(pin_red_dual, value_red_intensity_dual);
			analogWrite(pin_green_dual, value_green_intensity_dual);
		}
	}
}

int compute_value_for_dual_leds(){
  
  value_photoresistor = analogRead(pin_photoresistor);
  
  if(value_photoresistor > 1000){
    value_photoresistor = 1000; //asume 1000 is max (cu blitul in fata senzorului valoarea trece putin peste 1000);
  }

  float procent_intensitate_lumina = (value_photoresistor * 100)/1000;

  float procent_intensitate_leduri = 100 - procent_intensitate_lumina;
  int intensitate_leduri = (procent_intensitate_leduri * 255) / 100;

  return intensitate_leduri;
}

//2. Creste/scade intensitatea in functie de intensitatea luminii
    // port/sen/ph/is_off/end - este oprit
    // port/sen/ph/is_auto/end - este auto, seteaza singur valorile
    // port/sen/ph/man/r=value/a/g=value/end - manual
	
/*
	photoresistor-led/off -> sen/ph/is_off/end
	photoresistor-led/off/auto -> sen/ph/is_auto/end 
	photoresistor-led/manual/red={redvalue}/green={greenvalue} -> port/sen/ph/man/r=value/a/g=value/end
*/


void handle_request_intensitate_lumina_variabila(String request){
  if(request.indexOf("is_off") != -1){
    is_off_dual_led_photoresistor = true;
  }
  else if(request.indexOf("is_on") != -1){
    is_off_dual_led_photoresistor = false;
    if(request.indexOf("is_auto") != -1){
      is_auto_dual_led_photoresistor = true;
    }
    if(request.indexOf("man") != -1){
      is_auto_dual_led_photoresistor = false;

      String red_request = request.substring(request.indexOf("r="), request.indexOf("/a"));
      String green_request = request.substring(request.indexOf("g="));

      String string_valoare_led_red = parse_string_command(red_request, "=");
      String string_valoare_led_green = parse_string_command(green_request, "=");

      value_red_intensity_dual = string_valoare_led_red.toInt();
      value_green_intensity_dual = string_valoare_led_green.toInt();
    }
  }
  return;
}

handle2(){
	if(request.indexOf("is_off") != -1){
		is_off_dual_led_photoresistor = true;
	}
	else if(request.indexOf("is_on") != -1){
		is_off_dual_led_photoresistor = false;
	}
	else if(request.indexOf("is_auto") != -1 && is_off_dual_led_photoresistor == false){
      is_auto_dual_led_photoresistor = true;
    }
	else if(request.indexOf("man") != -1 && is_off_dual_led_photoresistor == false){
		is_auto_dual_led_photoresistor = false;
		
		String red_request = request.substring(request.indexOf("r="), request.indexOf("/a"));
		String green_request = request.substring(request.indexOf("g="));

		String string_valoare_led_red = parse_string_command(red_request, "=");
		String string_valoare_led_green = parse_string_command(green_request, "=");

		value_red_intensity_dual = string_valoare_led_red.toInt();
		value_green_intensity_dual = string_valoare_led_green.toInt();
	}
	
}
