#define DEBUG true
#include <ArduinoJson.h>
#include <OneWire.h>
#include <DallasTemperature.h>


/*
 * 1. Alarma incendiu 
 * foloseste KY-026 pentru a detecta flacara, KY-012 buzzer
 * Daca este detectata flacara atunci se declanseaza un sunet folosind buzzer-ul
 * Se trimite prin JSON si statusul, daca este detectat foc sau nu
 */
 int pin_senzor_detector_flacara = 53;
 int pin_buzzer_detector_flacara = 52;
 int valoare_detector_flacara;



/*
 * 2. Creste/scade intensitate led in functie de intensitatea luminii
 * uses KY-018 photoresistor and KY-029 dual led color (green and red) 
 * how it works: daca clientul selecteaza auto-mode, atunci intensitatea ledului este automat setata in functie de intensitatea luminii externe. Cu cat e mai luminos cu atat intensitatea ledului va fi mai scazuta 
 * Daca este selectat manual, atunci clientul va putea seta singur intensitatea culorii rosu si verde a ledului 
 * La rulare, default ledul este on si este pe auto
 */
 int pin_photoresistor = A0;
 int pin_red_dual = 8;
 int pin_green_dual = 9;
 int value_photoresistor = 0;
 boolean is_off_dual_led_photoresistor = false; //va fi setat din aplicatia client 
 boolean is_auto_dual_led_photoresistor = true; //vezi mai sus 
 int value_red_intensity_dual = 255;
 int value_green_intensity_dual = 255; //se vor trimite de la client 
 int value_intensity_auto = 0;
 int value_off = 0;
 int value_default = 255;


/*
 * 3. Control SMD RGB color intensity or set on auto 
 * A value for each R, G, B will come from the client
 * It can be turned on/off from a different switch button in the client application 
 * uses SMD RGB KY-009 and switch button
 */
 int pin_red_smd = 11;
 int pin_green_smd = 10;
 int pin_blue_smd = 12;
 int red_intensity = 255;
 int green_intensity = 255;
 int blue_intensity = 255;
 int pin_switch_smd = 22;
 int switch_state_smd = 1;
 int hold_time_smd = 0;
 int idle_time_smd = 0;
 int start_pressed_smd = 0;
 int end_pressed_smd = 0;
 int last_switch_state_smd = 0;
 int value_auto_smd_intensity;
 int smd_led_state = 0; //0 inseamna ca e stins, 1 e aprins, initial va fi stins 
 boolean is_auto_smd_light = false;
 boolean is_request = false;


/*
  * 5. Senzor light blocking KY-010 si led rosu
  * uses: sensor KY-010 - photo interrupted module will trigger a signal when light between the sensor's gap is blocked
  * in ideea aceasta, draperia va trece prin gap-ul senzoruli blocand lumina dintre cele doua capete si indicand ca este trasa
  * daca draperia este trasa, daca este setat mod aut atunci se va aprinde automat un led rosu, daca e mod manual atunci clientul decide 
  * daca se se aprinda sau nu
  * la fel si daca draperia nu e trasa (se considera ca patrunde lumina in dormitor)
  */
 int pin_light_blocking = 7;
 int pin_led_light_blocking = 2;
 int value_led_light_blocking;
 boolean is_auto_led_light_blocking = true;
 int value_led_light_blocking_from_client = 1;
 int value_sensor_light_blocking;
 

/*
 * 6. Turn on led with touch sensor 
 * uses KY-036 touch sensor
 */
 int pin_touch_sensor = 24;
 int pin_led_touch = 26;
 int value_touch_sensor = 0; //valoarea citita de la touch sensor
 int hold_time_touch_sensor = 0;
 int idle_time_touch_sensor = 0;
 int start_pressed_touch_sensor = 0;
 int end_pressed_touch_sensor = 0;
 int last_value_touch_sensor = 0;
 int value_led_state = 0;
 int value_led_touch;


/*
 * 7. PIR HC-SR505 mini detectie miscare + aprindere led la detectie 
 */
 int pin_pir_senzor = 50;
 int pin_pir_led = 3;
 int value_pir_led;

/*
 *8. KY-001 Temperature sensor using DallasTemperature library and OneWire
 */
 #define BUS_ONE_WIRE 5
 OneWire oneWire(BUS_ONE_WIRE);
 DallasTemperature sensor_temperature(&oneWire);
 float value_temperature;



 boolean sendOutputData;
  
 
//////////////////////////////////////////////////////////////SETUP FUNCTION//////////////////////////////////////////////////////////////////////////////////////////
void setup() {
  
  Serial.begin(9600);
  Serial1.begin(115200);

  //1. Alarma incendiu
  pinMode(pin_senzor_detector_flacara, INPUT);
  pinMode(pin_buzzer_detector_flacara, OUTPUT);

  //2. Dual led R&G cu intensitate reglata in functie de intensitatea luminii externe
  pinMode(pin_red_dual, OUTPUT);
  pinMode(pin_green_dual, OUTPUT);

  //3. Control SMD RGB with switch
  pinMode(pin_red_smd, OUTPUT);
  pinMode(pin_green_smd, OUTPUT);
  pinMode(pin_blue_smd, OUTPUT);
  pinMode(pin_switch_smd, INPUT_PULLUP);

  //5. light blocking sensor with led
  pinMode(pin_light_blocking, INPUT);
  pinMode(pin_led_light_blocking, OUTPUT);

  //6. turn on led with touch sensor
  pinMode(pin_touch_sensor, INPUT);
  pinMode(pin_led_touch, OUTPUT);

  //7.mini pir detectie miscare + led
  pinMode(pin_pir_senzor, INPUT);
  pinMode(pin_pir_led, OUTPUT);
  digitalWrite(pin_pir_led, LOW);

  //8. Temperature KY-001
  sensor_temperature.begin();


  //Test comenzi de la client
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  esp_initial_setup();
}


/////////////////////////////////////////////////////LOOP METHOD///////////////////////////////////////////////////////////////////////////////////////////////////////
DynamicJsonDocument add_data_to_json_server(){
  const int json_capacity = 192;
  DynamicJsonDocument doc(json_capacity);

  //alarma incendiu
  doc["senzor_incendiu"] = valoare_detector_flacara;

  //photoresistor and dual led
  JsonObject dual_led_photoresistor = doc.createNestedObject("dual_led_light_sensitive");
  dual_led_photoresistor["is_off_dual_led_photoresistor"] = is_off_dual_led_photoresistor;
  dual_led_photoresistor["is_auto_dual_led_photoresistor"] = is_auto_dual_led_photoresistor;
  dual_led_photoresistor["value_red_intensity_dual"] = value_red_intensity_dual;
  dual_led_photoresistor["value_green_intensity_dual"] = value_green_intensity_dual;
  dual_led_photoresistor["value_intensity_auto"] = value_intensity_auto;


  //smd rgb cu switch 
  JsonObject smd_control = doc.createNestedObject("smd_control");
  smd_control["switch_state_smd"] = smd_led_state;
  smd_control["red_intensity"] = red_intensity;
  smd_control["green_intensity"] = green_intensity;
  smd_control["blue_intensity"] = blue_intensity;
  smd_control["is_auto_smd_light"] = is_auto_smd_light;

  //led state touch
  doc["led_state_touch"] = value_led_touch;


  //light blocking sensor 
  doc["value_led_light_blocking"] = value_led_light_blocking;

  //pir detectie miscare
  doc["value_pir_led"] = value_pir_led;

  //temperature
  doc["value_temperature"] = value_temperature;
  
  return doc;
}

void loop() {
//  apelare functii pentru citire senzori
  sistem_alarma_incendiu();
  control_dual_led_with_extern_light_intensity();
  control_smd_rgb_with_switch();
 touch_sensor_led_control();
  light_blocking_sensor();
  pir_movement_detection();
  get_temperature();
  
  ////////trimitere date catre serverul modulului
  if(Serial1.available()){
    if(Serial1.find("+IPD,")){
//      delay(500);
      delay(20);
      int connectionId = Serial1.read() - 48; // functia read() returnează valori zecimale ASCII si caracterul ‘0’ are codul ASCII 48

      //serializare date de la senzori
      String output = "";
      serializeJson(add_data_to_json_server(), output);

      String header = "HTTP/1.1 200 OK\r\n";
      String cipSendHeader = "AT+CIPSEND=";
      cipSendHeader += connectionId;
      cipSendHeader += ",";
      cipSendHeader += header.length();
      cipSendHeader += "\r\n";
      sendData(cipSendHeader, 100, DEBUG);
      sendData(header, 150, DEBUG);
      
      String content = "Content-type:application/json\r\n";
      String contentheader = "AT+CIPSEND=";
      contentheader += connectionId;
      contentheader += ",";
      contentheader += content.length();
      contentheader += "\r\n";
      sendData(contentheader, 100, DEBUG);
      sendData(content, 150, DEBUG);
      
      String line = "\r\n";
      String contentLine = "AT+CIPSEND=";
      contentLine += connectionId;
      contentLine += ",";
      contentLine += line.length();
      contentLine += "\r\n";
      sendData(contentLine, 100, DEBUG);
      sendData(line, 150, DEBUG);
      
      String cipSend = "AT+CIPSEND=";
      cipSend += connectionId;
      cipSend += ",";
      cipSend += output.length();
      cipSend += "\r\n";
      sendData(cipSend, 100, DEBUG);
      
      //send data from sensors
      sendData(output, 150, DEBUG);
      
      
      String closeCommand = "AT+CIPCLOSE=";
      closeCommand += connectionId; 
      closeCommand += "\r\n";
      sendData(closeCommand, 300, DEBUG);
    }
  }
}




/////////////////////////////////////////////////////////////////////Preluare date senzori///////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////1. Alarma incendiu/////////////////////////////////////////////////////////////////////////
int sistem_alarma_incendiu(){
  valoare_detector_flacara = digitalRead(pin_senzor_detector_flacara);

  if(valoare_detector_flacara == HIGH){
    digitalWrite(pin_buzzer_detector_flacara, HIGH);
  }
  else {
    digitalWrite(pin_buzzer_detector_flacara, LOW);
  }
  delay(10);
  return valoare_detector_flacara;
}


//////////////////////////////////////////////////////////////////2. Dual led sensibil la lumina (photoresistor)///////////////////////////////////////////////////
void control_dual_led_with_extern_light_intensity(){
  if(is_off_dual_led_photoresistor == true){
    value_red_intensity_dual = value_off;
    value_green_intensity_dual = value_off;
    value_intensity_auto = value_off;
    analogWrite(pin_red_dual, value_off);
    analogWrite(pin_green_dual, value_off);
delay(10);
  }
  else{
    if(is_auto_dual_led_photoresistor == true){
      
      value_intensity_auto = compute_value_for_dual_leds();
      value_intensity_auto = 255 - value_intensity_auto;
      value_red_intensity_dual = value_intensity_auto;
      value_green_intensity_dual = value_intensity_auto;
      analogWrite(pin_red_dual, value_intensity_auto);
      analogWrite(pin_green_dual, value_intensity_auto);
delay(10);
    }
    else if(is_auto_dual_led_photoresistor == false){
      analogWrite(pin_red_dual, value_red_intensity_dual);
      analogWrite(pin_green_dual, value_green_intensity_dual);
delay(10);
    }
  }
}
int compute_value_for_dual_leds(){
  
  value_photoresistor = analogRead(pin_photoresistor);
  
  if(value_photoresistor > 1000){
    value_photoresistor = 1000; //asume 1000 is max (cu blitul in fata senzorului valoarea trece putin peste 1000);
  }

  float procent_intensitate_lumina = (value_photoresistor * 100)/1000;

  float procent_intensitate_leduri = 100 - procent_intensitate_lumina;
  int intensitate_leduri = (procent_intensitate_leduri * 255) / 100;

  return intensitate_leduri;
}




/////////////////////////////////////////////////////////////////////////3.SMD RGB with switch///////////////////////////////////////////////////////////////////
void control_smd_rgb_with_switch(){
  switch_state_smd = digitalRead(pin_switch_smd);
  if(switch_state_smd != last_switch_state_smd){
    update_switch_state_smd();
  }
  last_switch_state_smd = switch_state_smd;
  delay(10);
}
void update_switch_state_smd(){
  if(switch_state_smd == LOW){
    start_pressed_smd = millis();
  }
  else{
    end_pressed_smd = millis();
    hold_time_smd = end_pressed_smd - start_pressed_smd;

    if(hold_time_smd < 500){
//      Serial.println("Switch was pressed for less than half a second");
    }
    if(hold_time_smd >=500){
//      Serial.println("Switch was pressed for more than half a second");
      update_smd_state();
    }
  }
}
void update_smd_state(){
  if(is_request == false){
    //inseamna ca vine pentru ca a fost apasat
    smd_led_state == 0 ? smd_led_state = 1 : smd_led_state = 0;  
  
    if(smd_led_state == 0){
      //inseamna ca trebuie stins (starea curenta e aprins) 
      analogWrite(pin_red_smd, 0);  //set PWM value for red
      analogWrite(pin_blue_smd, 0); //set PWM value for blue
      analogWrite(pin_green_smd, 0); //set PWM value for green
      delay(10);
    }
    else if(smd_led_state == 1){
      if(is_auto_smd_light == true){
        auto_led_smd_rgb();
      }
      else if(is_auto_smd_light == false){
        analogWrite(pin_red_smd, red_intensity);  //set PWM value for red
        analogWrite(pin_blue_smd, blue_intensity); //set PWM value for blue
        analogWrite(pin_green_smd, green_intensity); //set PWM value for green
      }
    }
  }
  else {
   //trebuie actualizat pentru ca este un request
   if(is_auto_smd_light == true){
    auto_led_smd_rgb(); 
   }
   else {
    analogWrite(pin_red_smd, red_intensity);  //set PWM value for red
    analogWrite(pin_blue_smd, blue_intensity); //set PWM value for blue
    analogWrite(pin_green_smd, green_intensity); //set PWM value for green
   }
  }
}

void auto_led_smd_rgb(){
  int random_color = random(1,12);
   int color_selected;
   if(random_color >= 1 && random_color <= 4){
    color_selected = -1;
   }
   if(random_color >= 5 && random_color <= 8){
    color_selected = -2;
   }
   if(random_color >= 9 && random_color <= 12){
    color_selected = -3;
   }
   
   switch(color_selected){
    case -1:
      analogWrite(pin_red_smd, 255);
      analogWrite(pin_green_smd, 0);
      analogWrite(pin_blue_smd, 0);
      red_intensity = 255;
      blue_intensity = 0;
      green_intensity = 0;
      break;
     case -2:
      analogWrite(pin_green_smd, 255);
      analogWrite(pin_red_smd, 0);
      analogWrite(pin_blue_smd, 0);
      red_intensity = 0;
      blue_intensity = 0;
      green_intensity = 255;
      break;
     case -3:
      analogWrite(pin_blue_smd, 255);
      analogWrite(pin_red_smd, 0);
      analogWrite(pin_green_smd, 0);
      red_intensity = 0;
      blue_intensity = 255;
      green_intensity = 0;
      break;
     default:
      break;
   }
}


////////////////////////////////////////////////////////////////////////4. Sistem senzor soc KY-002//////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////5. Light blocking sensor with white led///////////////////////////////////////////////////
void light_blocking_sensor(){
  if(is_auto_led_light_blocking == true){
    value_sensor_light_blocking = digitalRead(pin_light_blocking);
    if(value_sensor_light_blocking == HIGH){
      
      digitalWrite(pin_led_light_blocking, HIGH);
      value_led_light_blocking = 1;
    }
    else if(value_sensor_light_blocking == LOW){
      digitalWrite(pin_led_light_blocking ,LOW);
      value_led_light_blocking = 0;
    }
  }
  else if(is_auto_led_light_blocking == false) {
    if(value_led_light_blocking_from_client == 1){
      digitalWrite(pin_led_light_blocking, HIGH);
      value_led_light_blocking = 1;
    }
    else if(value_led_light_blocking_from_client == 0){
      digitalWrite(pin_led_light_blocking, LOW);
      value_led_light_blocking = 0;
    }
  }
}


//////////////////////////////////////////////////////////////////////6. Turn on led with touch sensor////////////////////////////////////////////////////////////
void touch_sensor_led_control(){
 value_touch_sensor = digitalRead(pin_touch_sensor);
  
  if(value_touch_sensor!= last_value_touch_sensor){ //sensor state changed
    updateSensorState();
  }
  last_value_touch_sensor = value_touch_sensor;
}

void updateSensorState(){
  //the sensor has been pressed
  if(value_touch_sensor == HIGH){
    start_pressed_touch_sensor = millis();
  } else {
    end_pressed_touch_sensor = millis();
    hold_time_touch_sensor = end_pressed_touch_sensor - start_pressed_touch_sensor;

    if(hold_time_touch_sensor >= 200 && hold_time_touch_sensor < 500){
      //DO NOTHING, WILL TURN ON ONLY IF IT WAS MORE THAN A SECOND
    }
    if(hold_time_touch_sensor >= 500){
      //TURN ON IF IT WASS OFF OR OFF IF IT WAS ON
      updateLedState();
    }
  }
}

void updateLedState(){
  value_led_state == 0 ? value_led_state = 1 : value_led_state = 0;

  if(value_led_state == 0){
    digitalWrite(pin_led_touch, LOW);
    value_led_touch = 0;
    delay(50);
  }
  else if(value_led_state == 1){
    digitalWrite(pin_led_touch, HIGH);
    value_led_touch = 1;
    delay(50);
  }
}



///////////////////////////////////////////////////////////////////7. MINI pir movement detection//////////////////////////////////////////////////////////////////
void pir_movement_detection(){
   if(digitalRead(pin_pir_senzor) == HIGH){
    value_pir_led = 1;
    digitalWrite(pin_pir_led, HIGH);
   }
   else {
    value_pir_led = 0;
    digitalWrite(pin_pir_led, LOW);
   }
}


///////////////////////////////////////////////////////////////////////8. KY-001 Temperature/////////////////////////////////////////////////////////////////////
void get_temperature(){
  sensor_temperature.requestTemperatures();
  delay(5);
  value_temperature = sensor_temperature.getTempCByIndex(0);// You can have more than one IC on the same bus. 0 refers to the first IC on the wire 
}



/////////////////////////////////////////////////////////////////////////Configurare modul wifi ESP///////////////////////////////////////////////////////////////
void esp_initial_setup(){
  sendData("AT+RST\r\n", 2000, false); //restarteaza modulul
  sendData("AT+CWMODE=3\r\n", 1000, false); //seteaza modul de functionare al modulului, 3 inseamna ca functioneaza si ca SoftAP si station mode
  sendData("AT+CIFSR\r\n", 1000, DEBUG); //obtine o adresa IP locala pentru modul
  sendData("AT+CWSAP?\r\n", 2000, DEBUG); // citește informația
  sendData("AT+CIPMUX=1\r\n", 1000, false); //stabileste conexiune TCP, permite mai multe conexiuni (parametru = 0) inseamna ca permite doar o conexiune TCP
  sendData("AT+CIPSERVER=1,80\r\n", 1000, false); //creeaza serverul TCP, 1 = creeaza server, 0=sterge server, al doilea parametru specifica portul (cel default este 333)

  //https://www.espressif.com/sites/default/files/4a-esp8266_at_instruction_set_en_v1.5.4_0.pdf
}

String sendData(String command, const int timeout, boolean debug) 
{
  String response = "";
  Serial1.print(command); // trimite comanda la esp8266
  long int time = millis();
  while ((time + timeout) > millis()) {
    while (Serial1.available()) {
        char line = Serial1.read();
        delay(1);
        response += line;
    }
  }
    
    if(response.indexOf("/end") != -1){
      //GET /sen/ph/man/r=123/a/g=123

      int indexStartOfResponse = response.indexOf("GET");
      int indexEndOfResponse = response.indexOf("/end");
      String request = response.substring(indexStartOfResponse, indexEndOfResponse);
      
      parse_received_http_request(request);

    }
 if (debug) {
    Serial.println("Inceput Raspuns");
    Serial.print(response);
    Serial.println("Final Raspuns");
  }
  return response;

}

//numerotarea e in functie de ordinea in care au fost definite cele 7 comenzi la inceput (unele nu necesita primirea altor comenzi din partea utilizatorului)
void parse_received_http_request(String request){
  
  //http://192.168.4.1 - doar citeste
  //http://192.168.4.1/sen/ - inseamna ca urmeaza o comanda ce trebuie trimisa catre placa
  
  if(request.indexOf("sen" != -1)){
    
    //0. led placa (pentru testare functionalitate) - http://192.168.4.1/sen/led_placa=0/end (on) http://192.168.4.1/sen/led_placa=1/end (off)
    if(request.indexOf("led_placa") != -1) {
      handle_request_led_placa(request);
    }

    //2. Creste/scade intensitatea in functie de intensitatea luminii
    // port/sen/ph/is_off/end - este oprit
    // port/sen/ph/is_on/end - comanda de pornire
    // port/sen/ph/is_auto/end - seteaza pe auto
    // port/sen/ph/man/r=value/a/g=value/end
    if(request.indexOf("ph") != -1){
      handle_request_intensitate_lumina_variabila(request);
      return;
    }
    
    //3. SMD RGB cu swith fizic, control lumini din client (si stare on/off)
    //port/sen/smd/is_auto/end
    //port/sen/smd/man/r=value/a/g=value/b=value/end 
    if(request.indexOf("rgb") != -1){
      Serial.println(request);
      handle_request_smd_rgb(request);
      return;
    }
  }
}

//////////////////////////////////////////////////////////////////////////////////////REQUEST SMD RGB///////////////////////////////////////////////////////////////////////////////////////////////////
void handle_request_smd_rgb(String request){
//  16:23:20.184 -> GET /sen/smd/is_auto
//  16:23:25.297 -> GET /sen/smd/r=255/a/g=255/a/b=255
  if(request.indexOf("is_auto") != -1){
    is_auto_smd_light = true;
    is_request = true;
    update_smd_state();
    is_request = false;
    delay(5); return;
  }
  if(request.indexOf("is_auto") == -1){
    //vreau manual
    is_auto_smd_light = false;

    String red_request = request.substring(request.indexOf("r="), request.indexOf("/a"));
    String green_request = request.substring(request.indexOf("a/g="), request.indexOf("/a/b"));
    String blue_request = request.substring(request.indexOf("b="));
//17:58:48.909 -> GET /sen/rgb/r=0/a/g=0/a/b=255

    String string_valoare_red = parse_string_command(red_request, "=");
    String string_valoare_green = parse_string_command(green_request, "=");
    String string_valoare_blue = parse_string_command(blue_request, "=");

    red_intensity = string_valoare_red.toInt();
    green_intensity = string_valoare_green.toInt();
    blue_intensity = string_valoare_blue.toInt();

    is_request = true;
    update_smd_state();
    is_request = false;

    Serial.println(string_valoare_red);
    Serial.println(string_valoare_green);
    Serial.println(string_valoare_blue);
    delay(5); return;
  }
  return;
}

//////////////////////////////////////////////////////////////////////////////////////REQUEST LED CU FOTOREZISTOR////////////////////////////////////////////////////////////////////////////////////////
void handle_request_intensitate_lumina_variabila(String request){
  Serial.println(request);
  if(request.indexOf("is_off") != -1){
    Serial.println(request);
    is_off_dual_led_photoresistor = true;
    return;
  }
  else if(request.indexOf("is_on") != -1){
    is_off_dual_led_photoresistor = false;
    return;
  }
  else if(request.indexOf("is_auto") != -1 && is_off_dual_led_photoresistor == false){
      is_auto_dual_led_photoresistor = true;
      value_red_intensity_dual = value_intensity_auto;
      value_green_intensity_dual = value_intensity_auto;
      return;
    }
  else if(request.indexOf("man") != -1 && is_off_dual_led_photoresistor == false){
    is_auto_dual_led_photoresistor = false;
    
    String red_request = request.substring(request.indexOf("r="), request.indexOf("/a"));
    String green_request = request.substring(request.indexOf("g="));

    String string_valoare_led_red = parse_string_command(red_request, "=");
    String string_valoare_led_green = parse_string_command(green_request, "=");
    
    value_red_intensity_dual = string_valoare_led_red.toInt();
    value_green_intensity_dual = string_valoare_led_green.toInt();
    return;
  }
  return;
}
///////////////////////////////////////////////////////////////////////////////////////REQUEST LED PLACA//////////////////////////////////////////////////////////////////////////////////////////////////
void handle_request_led_placa(String request){
  int start_request = request.indexOf("led_placa");
  int end_request = request.indexOf("/end");
  
  String request_led = request.substring(start_request, end_request); //acum arata :led_placa=on / led_placa=off
  Serial.println(request_led);
  char delimiter[] = "=";
  
  String valoare_comanda_led = parse_string_command(request_led, delimiter);

  if(valoare_comanda_led.indexOf("on") != -1){
    digitalWrite(LED_BUILTIN, HIGH);
  }
  if(valoare_comanda_led.indexOf("off") != -1){
    digitalWrite(LED_BUILTIN, LOW);
  }
}

///////////////////////////////////////////////////////////////////////////////////PARSE REQUEST METHOD//////////////////////////////////////////////////////////////////////////////////////////////////////
//parseaza mesaj de tipul 'sensor=value'
String parse_string_command(String request, char delimiter[]){
  int i;
  char string[128];
  char *p;
  String words[2];
  request.toCharArray(string, sizeof(string));
  i = 0;
  p = strtok(string, delimiter);

  while(p && i < 2){
    words[i]=p;
    p=strtok(NULL, delimiter);
    ++i;
  }

//  for(i=0; i<2; i++){
//    Serial.println(words[i]);
//  }
//  Serial.println(words[1]);
  return words[1];
}
