/*
int pin_red_smd = 11;
int pin_green_smd = 10;
int pin_blue_smd = 12;

int red_intensity = 255;
int green_intensity = 255;
int blue_intensity = 255;

int pin_switch_smd = 22; pinul de la buton 
int switch_state_smd = 1;
int hold_time_smd = 0;
int idle_time_smd = 0;
int start_pressed_smd = 0;
int end_pressed_smd = 0;
int last_switch_state_smd = 0;

int value_auto_smd_intensity;
int smd_led_state = 0; 0 inseamna stins, 1 aprins, initial este stins 
boolean is_auto_smd_light = false 
boolean is_request = false;

JsonObject smd_control = doc.createNestedObject("smd_control");
  smd_control["switch_state_smd"] = smd_led_state;
  smd_control["red_intensity"] = red_intensity;
  smd_control["green_intensity"] = green_intensity;
  smd_control["blue_intensity"] = blue_intensity;
  smd_control["is_auto_smd_light"] = is_auto_smd_light;
*/

void control_smd_rgb_cu_switch(){
	switch_state_smd = digitalRead(pin_switch_smd);
	if(switch_state_smd != last_switch_state_smd){
		//inseamna ca este apasat sau s-a ridicat degetul de pe el 
		update_switch_state_smd();
	}
	last_switch_state_smd = switch_state_smd; //actualizam ultima stare
	delay(10);
}

void update_switch_state_smd(){
	if(switch_state_smd == LOW){
		//inseamna ca butonul este apasat, trebuie sa vad pentru cat timp 
		start_pressed_smd = millis();
	}
	else {
		end_pressed_smd = millis();
		hold_time_smd = end_pressed_smd - start_pressed_smd; //cat timp a fost apasat
		
		if(hold_time_smd < 500) {
			Serial.println("Switch was pressed for less than half a second");
		}
		if(hold_time_smd >= 500) {
			update_smd_state(); //daca e aprins il sting, daca e stins il aprind
		}
	}
}

void update_smd_state(){
	smd_led_state ==  ? smd_led_state = 1 : smd_led_state = 0;
	if(smd_led_state == 0){ //stins
		analogWrite(pin_red_smd, 0);
		analogWrite(pin_green_smd, 0);
		analogWrite(pin_blue_smd, 0);
	}
	else {
		if(is_auto_smd_light == true){
			auto_led_smd_rgb();
		}
		else if(is_auto_smd_light == false){
			analogWrite(pin_red_smd, red_intensity);
			analogWrite(pin_green_smd, green_intensity);
			analogWrite(pin_blue_smd, blue_intensity);
		}
	}
}

void auto_led_smd_rgb(){
  int random_color = random(1,12);
   int color_selected;
   if(random_color >= 1 && random_color <= 4){
    color_selected = -1;
   }
   if(random_color >= 5 && random_color <= 8){
    color_selected = -2;
   }
   if(random_color >= 9 && random_color <= 12){
    color_selected = -3;
   }
   
   switch(color_selected){
    case -1:
      analogWrite(pin_red_smd, 255);
      analogWrite(pin_green_smd, 0);
      analogWrite(pin_blue_smd, 0);
      red_intensity = 255;
      blue_intensity = 0;
      green_intensity = 0;
      break;
     case -2:
      analogWrite(pin_green_smd, 255);
      analogWrite(pin_red_smd, 0);
      analogWrite(pin_blue_smd, 0);
      red_intensity = 0;
      blue_intensity = 0;
      green_intensity = 255;
      break;
     case -3:
      analogWrite(pin_blue_smd, 255);
      analogWrite(pin_red_smd, 0);
      analogWrite(pin_green_smd, 0);
      red_intensity = 0;
      blue_intensity = 255;
      green_intensity = 0;
      break;
     default:
      break;
   }
}

void handle_request_smd_rgb(String request){
  int start_request = request.indexOf("smd");
  is_request = true;
  int end_request = request.indexOf("/end");
  String request_intensitate = request.substring(start_request, end_request); //smd_rgb/is_auto.../end
  if(request.indexOf("is_auto") != -1){
    is_auto_smd_light = true;
  }
  else if(request.indexOf("man") != -1){
    is_auto_smd_light = false;
    
    String red_request = request.substring(request.indexOf("r="), request.indexOf("&g"));
    String green_request = request.substring(request.indexOf("g="), request.indexOf("&b"));
    String blue_request = request.substring(request.indexOf("b="));

    String string_valoare_red = parse_string_command(red_request, "=");
    String string_valoare_green = parse_string_command(green_request, "=");
    String string_valoare_blue = parse_string_command(blue_request, "=");

    red_intensity = string_valoare_red.toInt();
    green_intensity = string_valoare_green.toInt();
    blue_intensity = string_valoare_green.toInt();

    return;
  }
}






/*
port/sen/smd/is_auto/end
port/sen/smd/man/end 
port/sen/smd/col/r=value/a/g=value/b=value/end 
*/
